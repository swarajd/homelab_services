package main

import (
	"flag"
	"log"
	"net"

	"gitlab.com/swarajd/homelab_services/mathlib"
	"gitlab.com/swarajd/homelab_services/mathsvc"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var grpcport = flag.String("grpcport", ":50051", "grpcport")

type server struct {
	mathsvc.UnimplementedMathServiceServer
}

func (s *server) CalcExpression(ctx context.Context, in *mathsvc.CalcExpressionRequest) (*mathsvc.CalcExpressionResponse, error) {
	result, err := mathlib.ParseExpression(in.Expression)

	var errStr string
	if err != nil {
		errStr = err.Error()
	} else {
		errStr = ""
	}

	return &mathsvc.CalcExpressionResponse{
		Result: result,
		Error:  errStr,
	}, nil
}

func main() {
	flag.Parse()

	if *grpcport == "" {
		flag.Usage()
		log.Fatalf("missing -grpcport flag (:50051)")
	}

	lis, err := net.Listen("tcp", *grpcport)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	sopts := []grpc.ServerOption{}

	s := grpc.NewServer(sopts...)
	mathsvc.RegisterMathServiceServer(s, &server{})
	reflection.Register(s)

	log.Printf("Starting server...")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
