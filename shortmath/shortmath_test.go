package main

import (
	"testing"
)

// TestBasic tests adding two numbers
func TestBasic(t *testing.T) {
	a := 1
	b := 1

	want := 2
	result := a + b

	if want != result {
		t.Fatalf("a + b = %q, want %q", result, want)
	}
}
