package mathlib

import (
	"fmt"
	"strconv"
	"strings"
)

func add(a float64, b float64) float64 {
	return a + b
}

func subtract(a float64, b float64) float64 {
	return a - b
}

func multiply(a float64, b float64) float64 {
	return a * b
}

func divide(a float64, b float64) float64 {
	return a / b
}

var operatorMap = map[string]func(float64, float64) float64{
	"+": add,
	"-": subtract,
	"*": multiply,
	"/": divide,
}

func ParseExpression(expr string) (float64, error) {
	stack := NewStack[float64]()

	for _, elem := range strings.Fields(expr) {
		if elem == "+" || elem == "-" || elem == "*" || elem == "/" {
			// check for operator
			if stack.IsEmpty() {
				return -1, fmt.Errorf("invalid expression: %q, no elements in stack: %v", expr, stack)
			} else {
				b, _ := stack.Pop()
				a, valid := stack.Pop()

				if !valid {
					return -1, fmt.Errorf("invalid expression: %q, not enough elements in stack: %v", expr, stack)
				} else {
					operation := operatorMap[elem]
					currentResult := operation(a, b)
					stack.Push(currentResult)
				}
			}
		} else {
			// check for number
			if _, err := strconv.Atoi(elem); err != nil {
				return -1, fmt.Errorf("%q is not a valid number", elem)
			} else {
				num, err := strconv.ParseFloat(elem, 64)
				if err != nil {
					return -1, fmt.Errorf("%q is not a valid number", elem)
				} else {
					stack.Push(num)
				}
			}
		}
	}

	result, valid := stack.Pop()
	if !valid || !stack.IsEmpty() {
		return -1, fmt.Errorf("invalid expression: %q", expr)
	}

	return result, nil
}
