package mathlib

type Stack[T any] struct {
	stack []T
}

func NewStack[T any]() *Stack[T] {
	return &Stack[T]{nil}
}

// IsEmpty: check if stack is empty
func (s *Stack[T]) IsEmpty() bool {
	return len(s.stack) == 0
}

// Push a new value onto the stack
func (s *Stack[T]) Push(elem T) {
	s.stack = append(s.stack, elem) // Simply append the new value to the end of the stack
}

// Remove and return top element of stack. Return false if stack is empty.
func (s *Stack[T]) Pop() (T, bool) {
	var x T
	if len(s.stack) > 0 {
		x, s.stack = s.stack[len(s.stack)-1], s.stack[:len(s.stack)-1]
		return x, true
	}
	return x, false
}
