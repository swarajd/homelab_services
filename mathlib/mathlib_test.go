package mathlib

import (
	"testing"
)

// TestParseExpression tests parseExpression
func TestParseExpression(t *testing.T) {
	expr := "1 1 +"

	want := 2.0
	result, err := ParseExpression(expr)
	if err != nil {
		t.Fatalf("parseExpression(%q) resulted in error %v", expr, err)
	}

	if want != result {
		t.Fatalf("parseExpression(%q) = %f, want %f", expr, result, want)
	}
}
