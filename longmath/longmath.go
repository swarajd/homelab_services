package main

// "example.com/main/mathlib"
// "github.com/confluentinc/confluent-kafka-go/v2/kafka"

import (
	"context"
	"fmt"
	"log"

	"github.com/segmentio/kafka-go"
	"gitlab.com/swarajd/homelab_services/mathlib"
)

func main() {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{"localhost:9092"},
		GroupID:  "longmath-consumer",
		Topic:    "math-expressions",
		MaxBytes: 10e6, // 10MB
	})

	for {
		msg, err := r.ReadMessage(context.Background())
		if err != nil {
			log.Fatalf("failed to consume message from topic: %v", err)
		}

		expr := string(msg.Value)

		result, err := mathlib.ParseExpression(expr)
		if err != nil {
			log.Fatalf("failed to parse math expression: %v", err)
		}

		fmt.Printf("expression: %v, result: %v\n", expr, result)
	}

	if err := r.Close(); err != nil {
		log.Fatal("failed to close reader:", err)
	}
}
